# augo

Very unoriginal project. Learning Go basics by porting an existing Ruby script.

augo can download packages, search packages, and look for updates on the AUR.
It can not *automatically* install or upgrade AUR packages.

## Building

    $ go build -o augo main.go

## Usage

Download a package:

    $ augo d qemu-patched

By default, this will save the package to `/storage/AUR`. This can be adjusted
in a configuration file at `~/.config/augo/augo.json` which is created on
first run if not available.

Search for packages (ordered by most votes first):

    $ augo s qemu

Print PKGBUILD for a package:

    $ augo p qemu-patched

Print all available info for a given package:

    $ augo i qemu-patched

Look for updates to local foreign packages:

    $ augo u

## Configuration

Example:

```json
{
    "SavePath": "/storage/AUR",
    "Ignores": ["mongodb", "wiredtiger", "dxvk-git", "dxvk-win32-git", "dxvk-win64-git"]
}
```

## License

See LICENSE.
