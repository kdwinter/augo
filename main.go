package main

//----------------------------------------------------------------------------

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"path"
	"sort"
	"strconv"
	"strings"
	"time"

	"github.com/mitchellh/colorstring"
)

//----------------------------------------------------------------------------

const (
	VERSION = "1.0.0"
	AUR_URL	= "https://aur.archlinux.org"
)

var (
	config Config
	flags = Flags{
		ShowIgnores: false,
	}
	configPath = path.Join(os.Getenv("XDG_CONFIG_HOME"), "augo/augo.json")
)

//----------------------------------------------------------------------------

type Config struct {
	SavePath	string		`json:"save_path"`
	Ignores		[]string	`json:"ignores"`
}

type Flags struct {
	ShowIgnores	bool
}

type Package struct {
	ID		uint32		`json:"ID"`
	Name		string		`json:"Name"`
	PackageBaseID	uint32		`json:"PackageBaseID"`
	PackageBase	string		`json:"PackageBase"`
	Version		string		`json:"Version"`
	Description	string		`json:"Description"`
	URL		string		`json:"URL"`
	NumVotes	uint32		`json:"NumVotes"`
	Popularity	float64		`json:"Popularity"`
	OutOfDate	int64		`json:"OutOfDate"`
	Maintainer	string		`json:"Maintainer"`
	FirstSubmitted	int64		`json:"FirstSubmitted"`
	LastModified	int64		`json:"LastModified"`
	URLPath		string		`json:"URLPath"`
	Depends		[]string	`json:"Depends"`
	MakeDepends	[]string	`json:"MakeDepends"`
	OptDepends	[]string	`json:"OptDepends"`
	Conflicts	[]string	`json:"Conflicts"`
	Provides	[]string	`json:"Provides"`
	Replaces	[]string	`json:"Replaces"`
	Groups		[]string	`json:"Groups"`
	License		[]string	`json:"License"`
	Keywords	[]string	`json:"Keywords"`
}

type AURResponse struct {
	Version		uint32		`json:"version"`
	ResultCount	uint32		`json:"resultcount"`
	Results		[]Package	`json:"results"`
}

type AURRequest struct {
	Type		string
	Parameter	string
	Value		string
}

//----------------------------------------------------------------------------

func GetPackageInfo(name string) Package {
	aurResponse, err := GetAURResponse(&AURRequest{Type: "info", Parameter: "arg", Value: url.QueryEscape(name)})
	if err != nil {
		Die(err)
	}

	return aurResponse.Results[0]
}

func PrintUsage(program string) {
	fmt.Printf("usage: %s [action] [argument]\n\n", program)
	fmt.Printf("actions:\n")
	fmt.Printf("    download|dl|d PKG      download PKG into %s\n", config.SavePath)
	fmt.Printf("    pkgbuild|p PKG         print PKGBUILD for PKG\n")
	fmt.Printf("    info|i PKG             print information about PKG\n")
	fmt.Printf("    search|s TERM          search AUR for TERM\n")
	fmt.Printf("    updates|u              check for updates to locally installed AUR packages\n")
}

func CompareVersion(v1, v2 string) int {
	_, err := exec.LookPath("vercmp")
	if err != nil {
		Die("vercmp not found on this system")
	}

	out, err := exec.Command("vercmp", v1, v2).Output()
	if err != nil {
		Die("couldn't compare version")
	}

	result, err := strconv.Atoi(strings.TrimSuffix(string(out), "\n"))
	if err != nil {
		Die("unexpected comparison output")
	}

	return result
}

func (pkg Package) PrintPKGBUILD() {
	httpClient  := &http.Client{Timeout: 5*time.Second}
	endpoint    := fmt.Sprintf("%s/cgit/aur.git/plain/PKGBUILD?h=%s", AUR_URL, pkg.Name)
	response, _ := httpClient.Get(endpoint)
	defer func() {
		if response != nil {
			response.Body.Close()
		}
	}()

	body, _ := ioutil.ReadAll(response.Body)
	fmt.Print(string(body))
}

func (pkg Package) Download() {
	colorstring.Printf("downloading [bold]%s[reset] into [bold]%s[reset]... ", pkg.Name, config.SavePath)

	cmd := exec.Command("git", "clone", fmt.Sprintf("%s/%s", AUR_URL, pkg.Name))
	cmd.Dir = config.SavePath
	if err := cmd.Run(); err != nil {
		Die("failed downloading package")
	}

	fmt.Println("done")
}

func (pkg Package) PrintInfo() {
	colorstring.Printf("AUR Page:       [bold]%s/packages/%s\n", AUR_URL, pkg.Name)
	colorstring.Printf("Version:        [bold]%s\n", pkg.Version)
	colorstring.Printf("Description:    [bold]%s\n", pkg.Description)
	colorstring.Printf("URL:            [bold]%s\n", pkg.URL)
	colorstring.Printf("Votes:          [bold]%d\n", pkg.NumVotes)
	colorstring.Printf("Popularity:     [bold]%f\n", pkg.Popularity)
	if pkg.OutOfDate > 0 {
		colorstring.Printf("OutOfDate:      since [bold]%s\n", time.Unix(pkg.OutOfDate, 0).UTC())
	}
	colorstring.Printf("Maintainer:     [bold]%s\n", pkg.Maintainer)
	colorstring.Printf("FirstSubmitted: [bold]%s\n", time.Unix(pkg.FirstSubmitted, 0).UTC())
	colorstring.Printf("LastModified:   [bold]%s\n", time.Unix(pkg.LastModified, 0).UTC())
	if len(pkg.Depends) > 0 {
		colorstring.Printf("Depends:        [bold]%s\n", strings.Join(pkg.Depends, ", "))
	}
	if len(pkg.MakeDepends) > 0 {
		colorstring.Printf("MakeDepends:    [bold]%s\n", strings.Join(pkg.MakeDepends, ", "))
	}
	if len(pkg.OptDepends) > 0 {
		colorstring.Printf("OptDepends:     [bold]%s\n", strings.Join(pkg.OptDepends, ", "))
	}
	if len(pkg.Conflicts) > 0 {
		colorstring.Printf("Conflicts:      [bold]%s\n", strings.Join(pkg.Conflicts, ", "))
	}
	if len(pkg.Provides) > 0 {
		colorstring.Printf("Provides:       [bold]%s\n", strings.Join(pkg.Provides, ", "))
	}
	if len(pkg.Replaces) > 0 {
		colorstring.Printf("Replaces:       [bold]%s\n", strings.Join(pkg.Replaces, ", "))
	}
	if len(pkg.Groups) > 0 {
		colorstring.Printf("Groups:         [bold]%s\n", strings.Join(pkg.Groups, ", "))
	}
	if len(pkg.License) > 0 {
		colorstring.Printf("License:        [bold]%s\n", strings.Join(pkg.License, ", "))
	}
}

type ByVotes []Package
func (a ByVotes) Len() int           { return len(a) }
func (a ByVotes) Less(i, j int) bool { return a[i].NumVotes > a[j].NumVotes }
func (a ByVotes) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }

func SearchPackages(term string) {
	aurResponse, err := GetAURResponse(&AURRequest{Type: "search", Parameter: "arg", Value: url.QueryEscape(term)})
	if err != nil {
		Die(err)
	}

	packages := aurResponse.Results
	sort.Sort(ByVotes(packages))

	for _, pkg := range packages {
		colorstring.Printf(
			"[bold][magenta]aur/[white]%s[reset] [green]%s[reset] (%d)\n    [white]%s\n",
			pkg.Name, pkg.Version, pkg.NumVotes, pkg.Description,
		)
	}
}

func CheckForeignPackageVersions() {
	_, err := exec.LookPath("pacman")
	if err != nil {
		Die("pacman not found on this system")
	}

	out, err := exec.Command("pacman", "-Qm").Output()
	if err != nil {
		Die("couldn't find local foreign packages")
	}

	var query []string
	localForeignPackages := make(map[string]Package)
	for _, line := range strings.Split(strings.TrimSuffix(string(out), "\n"), "\n") {
		name, ver := SplitPacmanLine(line, " ")
		if name != "" && ver != "" {
			localForeignPackages[name] = Package{Name: name, Version: ver}
			query = append(query, url.QueryEscape(name))
		}
	}

	queryString := strings.Join(query, "&arg[]=")
	aurResponse, err := GetAURResponse(&AURRequest{Type: "multiinfo", Parameter: "arg[]", Value: queryString})
	if err != nil {
		Die(err)
	}

	for _, remotePkg := range aurResponse.Results {
		if localPkg, ok := localForeignPackages[remotePkg.Name]; ok {
			if PackageIsIgnored(remotePkg.Name) {
				continue
			}

			if CompareVersion(localPkg.Version, remotePkg.Version) == -1 {
				colorstring.Printf(
					"[yellow]>[reset] [cyan]%s[reset] has an update available ([red]%s[reset] -> [green]%s[reset])\n",
					remotePkg.Name, localPkg.Version, remotePkg.Version,
				)
			} else {
				colorstring.Printf(
					"  [cyan]%s[reset] %s is up to date\n",
					remotePkg.Name, localPkg.Version,
				)
			}
		}
	}
}

func PackageIsIgnored(name string) bool {
	if flags.ShowIgnores {
		return false
	}

	for _, ignore := range config.Ignores {
		if name == ignore {
			return true
		}
	}

	return false
}

func SplitPacmanLine(s, sep string) (string, string) {
	x := strings.Split(s, sep)
	return x[0], x[1]
}

func Die(message interface{}) {
	colorstring.Printf("[red]%v\n", message)
	os.Exit(1)
}

func GetAURResponse(request *AURRequest) (AURResponse, error) {
	var aurResponse AURResponse

	httpClient := &http.Client{Timeout: 5*time.Second}
	endpoint   := fmt.Sprintf(
		"%s/rpc/?v=5&type=%s&%s=%s",
		AUR_URL, request.Type, request.Parameter, request.Value,
	)
	response, err := httpClient.Get(endpoint)
	if err != nil {
		return aurResponse, err
	}
	defer response.Body.Close()

	decoder := json.NewDecoder(response.Body)
	err = decoder.Decode(&aurResponse)
	if err != nil {
		return aurResponse, err
	}

	if aurResponse.ResultCount == 0 {
		return aurResponse, fmt.Errorf("no AUR results found")
	}

	return aurResponse, nil
}

func FormatErr(err error) string {
	return fmt.Sprintf("%s", err)
}

func CreateConfiguration() {
	if err := os.MkdirAll(path.Dir(configPath), 0755); err != nil {
		Die("couldn't create configuration directory")
	}

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		file, err := os.Create(configPath)
		if err != nil {
			Die("couldn't create configuration file")
		}
		defer file.Close()

		_, err = file.WriteString(`{
    "save_path": os.GetEnv("HOME") + "/AUR",
    "ignores": []
}`)
		if err != nil {
			Die("failed writing default configuration")
		}
	}
}

func ParseConfiguration() {
	file, err := os.Open(configPath)
	if err != nil {
		Die("couldn't open configuration file for reading")
	}
	defer file.Close()

	decoder := json.NewDecoder(file)
	err = decoder.Decode(&config)
	if err != nil {
		Die(err)
	}

	if _, err := os.Stat(config.SavePath); os.IsNotExist(err) {
		Die(fmt.Errorf("save_path '%s' from configuration does not exist", config.SavePath))
	}
}

func ParseArguments() {
	program := os.Args[0]
	args    := os.Args[1:]

	if len(args) > 0 {
		switch args[0] {
		case "download", "dl", "d":
			if len(args) > 1 {
				pkg := GetPackageInfo(args[1])
				pkg.Download()
			} else {
				Die("invalid package name provided")
			}
		case "pkgbuild", "p":
			if len(args) > 1 {
				pkg := GetPackageInfo(args[1])
				pkg.PrintPKGBUILD()
			} else {
				Die("invalid package name provided")
			}
		case "info", "i":
			if len(args) > 1 {
				pkg := GetPackageInfo(args[1])
				pkg.PrintInfo()
			} else {
				Die("invalid package name provided")
			}
		case "search", "s":
			if len(args) > 1 {
				SearchPackages(args[1])
			} else {
				Die("no search term provided")
			}
		case "updates", "u":
			if len(args) > 1 {
				if args[1] == "--show-ignores" {
					flags.ShowIgnores = true
				}
			}
			CheckForeignPackageVersions()
		case "help", "-h", "--help":
			PrintUsage(program)
		default:
			Die("invalid action provided")
		}
	} else {
		PrintUsage(program)
	}
}

//----------------------------------------------------------------------------

func main() {
	CreateConfiguration()
	ParseConfiguration()
	ParseArguments()
}
